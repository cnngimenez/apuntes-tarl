#! /usr/bin/fish

if test -z "$NETKIT_HOME"
    set -x NETKIT_HOME "$PWD"
    echo "Asignado NETKIT_HOME  a: $NETKIT_HOME"
    if test -f "$NETKIT_HOME/bin/vstart"
        echo "Bien, vstart encontrado."
    else
        echo "¡vstart no fue encontrado!"
        echo "¡Este script debe estar dentro del directorio netkit!"
        exit 1
    end
else
    echo "NETKIT_HOME  ya está asignado a: $NETKIT_HOME"
end

set -x PATH $PATH "$NETKIT_HOME/bin" "$NETKIT_HOME/netgui/bin"
set -x MANPATH $MANPATH "" "$NETKIT_HOME/man"

if test -z "$JAVA_HOME"
    echo "Buscando máquina virtual de java..."
    set java_bin (find /usr/lib/jvm -name 'java' -printf '%h\n' | head -n 1)
    set -x JAVA_HOME (dirname "$java_bin")
    echo "Encontrado en: $java_bin"
    echo "Asignado JAVA_HOME    a: $JAVA_HOME"
else
    echo "JAVA_HOME    ya está asignado a: $JAVA_HOME"
end

./check_configuration.sh

#! /bin/bash

export NETKIT_HOME="$PWD"
export PATH=$PATH:"$NETKIT_HOME/bin":"$NETKIT_HOME/netgui/bin"
export MANPATH=$MANPATH::"$NETKIT_HOME/man"
java_bin=`find /usr/lib/jvm -name 'java' -printf '%h\n' | head -n 1`
export JAVA_HOME=`ls -1d /usr/lib/jvm/java-8* | head -n 1`
./check_configuration.sh
